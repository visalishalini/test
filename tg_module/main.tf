provider "aws" {
  profile = "Account1"  # The AWS CLI profile for the target account
  region  = "us-east-1" # Change to your desired region
}

module "tgw_attachment" {
  source = "./modules"

  # shared_services_account_id = "123456789012" # Shared Services Account ID
  transit_gateway_id = "tgw-0081c59b7ea296427"      # Replace with the actual Transit Gateway ID
  vpc_id             = "vpc-0cd3c23102e0e063d"      # Replace with the actual VPC ID
  subnet_ids         = [ "subnet-0f25f72903a99a187" ] # Replace with the actual subnet IDs
  #resource_share_arn         = "arn:aws:ram:us-east-1:123456789012:resource-share/resource-share-id" # Replace with the actual ARN
  #account_id                 = "098765432109" # Target Account ID
}


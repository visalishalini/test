variable "transit_gateway_id" {
  description = "The ID of the Transit Gateway."
  type        = string
}

variable "vpc_id" {
  description = "The ID of the VPC to attach to the Transit Gateway."
  type        = string
}

variable "subnet_ids" {
  description = "A list of subnet IDs to attach to the Transit Gateway."
  type        = list(string)
}

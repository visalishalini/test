provider "aws" {
  profile = "Account1"
  region = "us-east-1" # Change to your desired region
}

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw_attachment" {
  transit_gateway_id = var.transit_gateway_id
  vpc_id             = var.vpc_id
  subnet_ids         = var.subnet_ids

  tags = {
    Name = "Acc1-TGW-Attachment"
  }
}


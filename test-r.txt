from sys import platform
from src.services.promise_service import PromiseService
import os
import subprocess
import re
import base64
import requests
import shutil
import logging
import gitlab

def create_request():
    print('----create RDS request----')
    commit_message = f'{promise_name} and {request_name}'
    print(f"commit_message: {commit_message}")

    # Define the input and output file paths
    input_file = "/tmp/scripts/rdspostgresql_tf.template"
    output_file = "/tmp/scripts/rdspostgresql_ammended.tf"

    # Define the placeholders and their replacements
    placeholders = {
        "MODULENAMEPLACEHOLDER": f"rdspostgresql-{identifier}-module", 
        "IDENTIFIERPLACEHOLDER": identifier
    }

    # Check if the input file exists
    if os.path.isfile(input_file):
        with open(input_file, "r") as file:
            content = file.read()

        # Perform text substitution using re.sub()
        for placeholder, replacement in placeholders.items():
            content = re.sub(placeholder, replacement, content)

        # Write the modified content to the output file
        with open(output_file, "w") as file:
            file.write(content)
    else:
        print(f"{input_file} does not exist.")
   
    # Open the file and read its contents
    with open('/tmp/scripts/rdspostgresql_ammended.tf', 'rb') as file:
        file_content = base64.b64encode(file.read()).decode()

    # Print the decoded content
    print("file_content:")
    print(file_content)


    merge_title = f'Request: {request_name} for rdspostgresql promise'
    target_branch = 'main'

    #Now check the code into gitlab

    # Initialize GitLab instance
    gl = gitlab.Gitlab('https://gitlab.com', private_token=token)

    # Get the project
    project = gl.projects.get(PROJECT_ID)

    # Create a new commit with the file
    json_data = {
        'branch': 'main',
        'commit_message': 'Upload testfile',
        'actions': [
            {
                'action': 'create',
                'file_path': tf_file_path,
                'content': file_content,
                'encoding': 'base64'
            }
        ]
    }

    logging.basicConfig(level=logging.DEBUG)

    try:
        commit = project.commits.create(json_data)
        print("Commit created successfully!")
    except GitlabCreateError as e:
        print(f"Error creating commit: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")

    print("debug")

    # Read the contents of the file
    with open('/tmp/scripts/status.yaml', 'r') as file:
        contents = file.read()

    # Replace the string
    new_contents = re.sub(r'TMPIDENTIFIER', '${identifier}', contents, flags=re.MULTILINE)

    ## Write the new contents to a new file
    with open('/tmp/scripts/newstatus.yaml', 'w') as file:
        file.write(new_contents)

    ## Copy the new file to the destination
    shutil.copy('/tmp/scripts/newstatus.yaml', '/kratix/metadata/status.yaml')

"""
def copy_output_files():
    os.system('cp src/files/outputs/* /kratix/output/')
"""

promise_service = PromiseService()
input_file_path = '/kratix/input/object.yaml'
identifier = promise_service.get_property(input_file_path, 'spec', 'identifier')
connection_security_group = promise_service.get_property(input_file_path, 'spec', 'connection_security_group')
request_name = promise_service.get_property(input_file_path, 'metadata', 'name')
promise_name = 'rdspostgresql'
tf_file_path = f'rdspostgresql_request_{promise_name}_{request_name}.tf'
branch = 'main'
#PROJECT_ID = 'tfeworkspaces/kratixtfepocrepo'    
PROJECT_ID = 'tfeworkspaces/testworkspaces/terraform-aws-paap-rds-workspace'    
token = os.environ["GITLAB_TOKEN"]

print(f"token: {token}")
print(f"KRATIX_WORKFLOW_ACTION:")
print(os.environ.get('KRATIX_WORKFLOW_ACTION'))
print(f"identifier: {identifier}")
print(f"connection_security_group: {connection_security_group}")
print(f"request_name: {request_name}")
print(f"promise_name: {promise_name}")
print(f"tf_file_path: {tf_file_path}")
print(f"PROJECT_ID: {PROJECT_ID}")


workflow_action = os.environ.get("KRATIX_WORKFLOW_ACTION")
if workflow_action == "configure":
    create_request()
elif workflow_action == "delete":
    gl      = gitlab.Gitlab('https://gitlab.com', private_token=token)
    project = gl.projects.get(PROJECT_ID)

    project.files.delete(file_path=tf_file_path, commit_message=f'Delete {tf_file_path}', branch='main')

               
#copy_output_files()